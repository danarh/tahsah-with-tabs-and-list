package com.example.user.mytabs

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.RatingBar
import com.google.firebase.database.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_temp.*

class MainActivity : AppCompatActivity() {

    var firebaseStore: FirebaseFirestore = FirebaseFirestore.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val fragmentAdapter = MyPagerAdapter(supportFragmentManager)
        viewpager_main.adapter = fragmentAdapter

        tabs_main.setupWithViewPager(viewpager_main)

        //arraylist
//        var places = ArrayList<Place>()
//
//        places.add(
//            Place(
//                "Ward Resturant",
//                "Discription",
//                "https://media-cdn.tripadvisor.com/media/photo-s/05/b8/80/25/ward.jpg"
//            )
//        )
//        places.add(
//            Place(
//                "InDoor Cafe",
//                "Discription",
//
//                "http://imgsrv2.pxdrive.com/pics/norm/281449.jpg"
//            )
//        )
//        places.add(
//            Place(
//                "java u",
//                "Discription",
//                "https://pbs.twimg.com/media/B-xbaBjUMAAVGAn.jpg"
//            )
//        )
//
//        places.add(
//            Place(
//                "xxxxxxxx",
//                "Discription",
//                "https://www.fakenamegenerator.com/images/sil-female.png"
//            )
//        )
        //firebase
//        val settings = FirebaseFirestoreSettings.Builder()
//            .setTimestampsInSnapshotsEnabled(true)
//            .build()
//        firebaseStore.setFirestoreSettings(settings)
//        var name: String = mRestName.text.toString().trim()
//        val values = HashMap<String, ArrayList<Place>>()
//        values.put("place_name", places)
//
//        firebaseStore.collection("Places")
//            .add(values as Map<String, Any>)
//            .addOnSuccessListener { documentReference ->
//
//            }
    }
}

